#include <ESP8266WiFi.h>
#include <PubSubClient.h>             
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

// Update these with values suitable for your network.

#define PIR_PIN 3

int pirState = 0;
int lightState = 0;
int airconState = 0;

unsigned long previousMillis = 0;
long OffTime = 10000;
long OnTime = 10000;

const char* ssid = "SC_NIGHTOWL_LAB";
const char* password = "a78ae1be68";
const char* mqtt_server = "10.7.1.1";
const char* mqtt_username = "smart_classroom";
const char* mqtt_password = "FF4BpcMHZVb9dCBVTRBq";
const char* mqtt_id = "Capstone-PIR";
const char* publish_msg = "smartclassroom/NightOwl/light-pir/status";
const char* publish_msg2 = "smartclassroom/NightOwl/aircon-pir/status";
const char* subscribe_lights = "smartclassroom/NightOwl/lights_btn/check_status";
const char* subscribe_aircon = "smartclassroom/NightOwl/aircon_btn/check_status";

WiFiClient espClient;
PubSubClient client(espClient);

char button_value[50];

void setup() {
  // Initialize the PIR pin as an INPUT
  Serial.begin(115200,SERIAL_8N1,SERIAL_TX_ONLY);
  pinMode(PIR_PIN, INPUT_PULLUP);
  
  ArduinoOTA.setPort(8266);
  ArduinoOTA.setHostname("PIR");
  ArduinoOTA.setPassword((const char *)"xai0aeQu2g");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  reconnect();
  Serial.println(F("-------------------"));
  Serial.println(F("Everything Ready"));
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  memset(button_value, 0, sizeof(button_value));
  strncpy(button_value, (char *)payload, length);

  if (strcmp(button_value, "true") == 0) {
    //lights check button status for true
    lightState = HIGH;
  }
  else if (strcmp(button_value, "false") == 0) {
    //lights check button status for false
    lightState = LOW;
  }
  if (strcmp(button_value, "1") == 0) {
    //aircon check button status for true
    airconState = HIGH;
  }
  else if (strcmp(button_value, "0") == 0) {
    //aircon check button status for false
    airconState = LOW;
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(mqtt_id, mqtt_username, mqtt_password)) {
      client.subscribe(subscribe_lights);
      client.subscribe(subscribe_aircon);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

bool ota_flag = true;
uint16_t time_elapsed = 0;

void loop() {
    if (ota_flag)
    {
      while (time_elapsed < 30000)
      {
  ArduinoOTA.handle();
        time_elapsed = millis();
        delay(10);
      }
      ota_flag = false;
    }
  delay(500);
  
  pir_detection();

  if (!client.connected()) {
    reconnect();
  }

  client.loop();
  delay(500);
}

void pir_detection() {
  Serial.println("..");
  Serial.println(lightState);
  Serial.println("/");
  Serial.println(airconState);
  Serial.println("..");
  
  unsigned long currentMillis = millis();
  
  pirState = digitalRead(PIR_PIN);
  Serial.println(pirState);
  if (pirState == LOW) {
//    client.setCallback(callback);
    if ((lightState == HIGH) && (currentMillis - previousMillis >= OffTime)) {
      client.publish(publish_msg, "false");
      lightState = LOW;
    } if (airconState == HIGH) {
      client.publish(publish_msg2, "false");
      airconState = LOW;
    }
    previousMillis = currentMillis;
  }
  else if ((pirState == HIGH) && (currentMillis - previousMillis >= OnTime)) {
//    client.setCallback(callback);
    client.publish(publish_msg, "detected");
    previousMillis = currentMillis;
  }
}
