#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

// Update these with values suitable for your network.

#define SMOKEDETECTOR_PIN 3

int smoke_detectorState = 0;

const char* ssid = "SC_NIGHTOWL_LAB";
const char* password = "a78ae1be68";
const char* mqtt_server = "10.7.1.1";
const char* mqtt_username = "smart_classroom";
const char* mqtt_password = "FF4BpcMHZVb9dCBVTRBq";
const char* mqtt_id = "Capstone-SMOKEDETECTOR";
const char* publish_msg = "smartclassroom/NightOwl/smoke_detector/status";
const char* subscribe_smoke_detector = "smartclassroom/NightOwl/smoke_detector/check_status";

WiFiClient espClient;
PubSubClient client(espClient);

char button_value[50];

void setup() {
  // Initialize the pin as an INPUT
  Serial.begin(115200,SERIAL_8N1,SERIAL_TX_ONLY);
  pinMode(SMOKEDETECTOR_PIN, INPUT);
  
  ArduinoOTA.setPort(8266);
  ArduinoOTA.setHostname("SMOKEDETECTOR");
  ArduinoOTA.setPassword((const char *)"xai0aeQu2g");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  reconnect();
  Serial.println(F("-------------------"));
  Serial.println(F("Everything Ready"));
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  memset(button_value, 0, sizeof(button_value));
  strncpy(button_value, (char *)payload, length);

  if (strcmp(button_value, "true") == 0) {
  }
  else if (strcmp(button_value, "false") == 0) {
    client.publish(publish_msg, "false");
    digitalWrite(SMOKEDETECTOR_PIN, LOW);
//    ESP.reset();
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(mqtt_id, mqtt_username, mqtt_password)) {
      client.subscribe(subscribe_smoke_detector);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

bool ota_flag = true;
uint16_t time_elapsed = 0;

void loop() {
    if (ota_flag)
    {
      while (time_elapsed < 30000)
      {
  ArduinoOTA.handle();
        time_elapsed = millis();
        delay(10);
      }
      ota_flag = false;
    }
  delay(500);

  smoke_detector();

  if (!client.connected()) {
    reconnect();
  }

  client.loop();
  delay(500);
}

void smoke_detector() {
  smoke_detectorState = digitalRead(SMOKEDETECTOR_PIN);
  Serial.println(smoke_detectorState);
  if (smoke_detectorState == HIGH) {
    client.publish(publish_msg, "true");
    delay(3000);
  }
  else if(smoke_detectorState == LOW) {
    client.publish(publish_msg, "false");
  }
}